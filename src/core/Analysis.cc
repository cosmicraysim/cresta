// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
#include "Analysis.hh"
#include "G4Headers.hh"
#include "SystemHeaders.hh"

#include "Analysis.hh"
#include "VDetector.hh"
#include "VFluxProcessor.hh"
#include "VProcessor.hh"
#include "VTrigger.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Core { // CRESTA Core Sub Namespace

// ---------------------------------------------------------
// Constructors
// ---------------------------------------------------------
Analysis::Analysis() :
  fFluxProcessor(0),
  fSavedEvents(0),
  fGeneratedEvents(0),
  fNTuplesSetup(0),
  fChunkSize(10000),
  fRunMode(kEventMode),
  fLastCount(0),
  fStartTime(0),
  fTotalEvents(0)
{
}

Analysis::~Analysis()
{
}

// ---------------------------------------------------------
// Singleton Access
// ---------------------------------------------------------
Analysis *Analysis::fPrimary(0);

// ---------------------------------------------------------
// COUNTERS / IO
// ---------------------------------------------------------
void Analysis::CheckAbortState() {
  if (fRunMode == kTimeExposureMode) {
    if (GetExposureTime() > fRequiredExposure) {
      G4RunManager::GetRunManager()->AbortRun(true);
    }
  }
  if (fRunMode == kTriggerMode) {
    if (fSavedEvents > fRequiredTriggers) {
      G4cout << "RUN: Checking Abort State" << G4endl;
      G4RunManager::GetRunManager()->AbortRun(true);
    }
  }
  return;
}

void Analysis::ResetCounters() {
  fSavedEvents = 0;
  fGeneratedEvents = 0;
  fFluxProcessor->ResetExposureTime();
}

double Analysis::GetEventRate() {
  return fFluxProcessor->GetEventRate();
}

void Analysis::PrintProgress(int curcount, int totalcount) {

  // Only print every 1000 events regardless
  if (curcount % 1000 != 0) return;

  // Get time taken so far
  long int curtime = time(0);
  long int prctime = curtime - fStartTime;

  std::cout << "ACT: SubEvt : " << std::setfill(' ') << std::setw(6) << curcount << " Trg : " << std::setfill(' ') << std::setw(6) << fSavedEvents;
  G4double exptime = GetExposureTime();
  std::cout << " Exp : " << int(exptime) << "s";
  std::cout << " (" << std::setprecision(3) << exptime/60.0 << "min)";
  std::cout << " Rat (Hz) : " << std::setfill(' ') << std::setw(5) << std::setprecision(3) << float(fSavedEvents)/exptime;

  // Print N Events
  if (curcount > 0){
    if (fRunMode == kEventMode) {

      double remtime = double(fRequiredEvents * prctime / double(fGeneratedEvents));
      std::cout << " --> " << std::setprecision(3) << prctime/60.0 << "/"
                           << std::setprecision(3) << remtime/60.0 << "min" << std::endl;
      fLastCount = curcount;

    } else if (fRunMode == kTimeExposureMode) {

      double remtime = double(double(fRequiredExposure) * prctime / exptime);
      std::cout << " --> " << std::setprecision(3) << prctime/60.0 << "/"
                           << std::setprecision(3) << remtime/60.0 << "min" << std::endl;
      fLastCount = exptime;

    } else if (fRunMode == kTriggerMode) {

      double remtime = double(double(fRequiredTriggers) * prctime / double(fSavedEvents));
      std::cout << " --> " << std::setprecision(3) << prctime/60.0 << "/"
                           << std::setprecision(3) << remtime/60.0 << "min" << std::endl;
      fLastCount = fSavedEvents;

    }
  } else {
    std::cout << std::endl;
  }
}


void Analysis::InitialSetup(){
  fNTuplesSetup = true;
  std::cout << "ANL: Setting up G4Manager" << std::endl;
  // Initialise G4 ROOT Manager
  fG4Manager = G4AnalysisManager::Instance();
  fG4Manager->SetVerboseLevel(0);
  fG4Manager->SetFirstNtupleId(0);
  fG4Manager->SetFirstHistoId(0);
  fG4Manager->CreateNtuple("detectorevents", "Dynamic Detector Saved Information");

  // Run event processors begin run action
  std::vector<VProcessor*>::iterator iter;
  for (iter = fProcessors.begin(); iter != fProcessors.end(); iter++) {
    (*iter)->BeginOfRunAction(NULL);
  }

  // Run Flux Processor begin run action
  if (fFluxProcessor) fFluxProcessor->BeginOfRunAction(NULL);

  // Finish Ntuple and open output
  fG4Manager->FinishNtuple();
}

void Analysis::MakeRunFolder(){
  // Make a new run folder if not one.
  std::string foldername = "";
  foldername += fOutputTag;
  // foldername += std::to_string(fRunID);
  DIR *dir;
  dir = opendir(foldername.c_str());
  if(dir==NULL)
  {
    mkdir(foldername.c_str(), 0777);
  }
}


// ---------------------------------------------------------
// Main Run Processing
// ---------------------------------------------------------
void Analysis::BeginOfRunAction(const G4Run * run) {

  // Make a new run folder if not one.
  std::string foldername = "";
  foldername += fOutputTag;

  // Make new output file in the folder.
  std::string outputname = "";
  outputname += foldername + "/";
  outputname += fOutputTag + ".";
  outputname += std::to_string(fRunID) + ".";
  outputname += std::to_string(fSubRunID) + ".root";

  MakeRunFolder();

  // Setup Ntuples
  if (!fNTuplesSetup) {
    InitialSetup();
  }


  // Open new output
  fG4Manager->OpenFile(outputname);

  // If SubRunID == 0, save a config too
  std::string configname = "";
  configname += foldername + "/";
  configname += fOutputTag;
  configname += ".config";

  std::ofstream configfile;
  configfile.open(configname, std::ios::out);
  configfile << (DB::Get()->GetConstantList());
  configfile.close();
}

void Analysis::EndOfRunAction(const G4Run* /*run*/) {
  // Skip Dummy Runs
  fG4Manager->Write();
  fG4Manager->CloseFile();

}

void Analysis::BeginOfEventAction() {
  ResetState();
}

void Analysis::ProcessEvent(const G4Event * event) {

  // Run Flux Processor
  if (fFluxProcessor) fFluxProcessor->ProcessEvent(event);

  // Run Event processors
  std::vector<VProcessor*>::iterator piter;
  for (piter = fProcessors.begin(); piter != fProcessors.end(); piter++) {
    (*piter)->ProcessEvent(event);
  }

  // Run Trigger Processors
  std::vector<VTrigger*>::iterator titer;
  int trig_counts=0;
  for (titer = fTriggers.begin(); titer != fTriggers.end(); titer++) {
    (*titer)->ProcessTrigger(event);
    if ((*titer)->GetTrigger()) {
      fTriggersCounts[trig_counts]++;

    }

    trig_counts++;
  }

  // Add one to total generated
  fGeneratedEvents++;

  // If we have at least one trigger record the event
  if (IsTriggered()) RecordEvent();
}

void Analysis::RecordEvent() {
  fG4Manager->AddNtupleRow();
  fSavedEvents++;
}

void Analysis::ResetState() {
  ResetTriggers();
  ResetProcessors();
  ResetDetectors();
}

// ---------------------------------------------------------
// TRIGGERS
// ---------------------------------------------------------
void Analysis::RegisterTrigger(VTrigger * t) {
  fTriggers.push_back(t);
  fTriggersCounts.push_back(0);
}

bool Analysis::IsTriggered() {

  // If no valid triggers loaded, save everything
  if (fTriggers.size() < 1) return true;

  // For now just find at least one true trigger
  std::vector<VTrigger*>::iterator iter;
  for (iter = fTriggers.begin(); iter != fTriggers.end(); iter++) {
    if ((*iter)->GetTrigger()) {
      return true;
    }
  }

  return false;
}

void Analysis::ResetTriggers() {
  std::vector<VTrigger*>::iterator iter;

  for (iter = fTriggers.begin(); iter != fTriggers.end(); iter++) {
    (*iter)->Reset();
  }

  // Reset the trigger counts too

}

void Analysis::DestroyTriggers() {
  for (uint i = 0; i < fTriggers.size(); i++) {
    delete (fTriggers[i]);
  }
}

VTrigger* Analysis::GetTrigger(std::string id, bool silentfail) {

  std::vector<VTrigger*>::iterator iter;
  for (iter = fTriggers.begin(); iter != fTriggers.end(); iter++) {
    std::string trigid = (*iter)->GetID();
    if (id.compare(trigid) == 0) return (*iter);
  }
  if (!silentfail) {
    std::cout << "Cannot find Trigger : " << id << std::endl;
    throw;
  }
  return 0;
}



// ---------------------------------------------------------
// PROCESSORS
// ---------------------------------------------------------
void Analysis::RegisterProcessor(VProcessor * p) {
  fProcessors.push_back(p);
}

void Analysis::ResetProcessors() {
  std::vector<VProcessor*>::iterator iter;
  for (iter = fProcessors.begin(); iter != fProcessors.end(); iter++) {
    (*iter)->Reset();
  }
}

void Analysis::DestroyProcessors() {
  for (uint i = 0; i < fProcessors.size(); i++) {
    delete fProcessors[i];
  }
}

VProcessor* Analysis::GetProcessor(std::string id, bool silentfail) {
  std::vector<VProcessor*>::iterator iter;
  for (iter = fProcessors.begin(); iter != fProcessors.end(); iter++) {
    std::string procid = (*iter)->GetID();
    if (id.compare(procid) == 0) return (*iter);
  }
  if (!silentfail) {
    std::cout << "Cannot find Processor : " << id << std::endl;
    throw;
  }
  return 0;
}


//------------------------------------------------------
// DETECTORS
// ---------------------------------------------------------
void Analysis::RegisterDetector(VDetector * p) {
  fDetectors.push_back(p);
  G4SDManager::GetSDMpointer()->AddNewDetector(static_cast<G4VSensitiveDetector*>(p));
}

void Analysis::ResetDetectors() {
  std::vector<VDetector*>::iterator iter;
  for (iter = fDetectors.begin(); iter != fDetectors.end(); iter++) {
    (*iter)->ResetState();
  }
}

void Analysis::DestroyDetectors() {
  for (uint i = 0; i < fDetectors.size(); i++) {
    delete fDetectors[i];
  }
}

VDetector* Analysis::GetDetector(std::string id, bool silentfail) {
  std::vector<VDetector*>::iterator iter;
  for (iter = fDetectors.begin(); iter != fDetectors.end(); iter++) {
    std::string detid = (*iter)->GetID();
    if (id.compare(detid) == 0) return (*iter);
  }
  if (!silentfail) {
    std::cout << "Cannot find Detector : " << id << std::endl;
    throw;
  }
  return 0;
}



// ---------------------------------------------------------
// VISUALISATION
// ---------------------------------------------------------
G4VVisManager* Analysis::GetVisManager() {
  if (fInteractive) {
    G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
    return pVVisManager;
  } else {
    return NULL;
  }
}


// ---------------------------------------------------------
// OTHER
// ---------------------------------------------------------

void Analysis::BuildMCMap() {
  //
  // // Make new output file
  // std::string outputname = "";
  // outputname += fOutputTag + ".";
  // outputname += std::to_string(fRunID) + ".mcmap.root";
  // std::cout << "[ANA ]: Saving MCMap to : " << outputname << std::endl;
  //
  // // Open New File
  // TFile* mapfile = new TFile(outputname.c_str(), "RECREATE");
  // mapfile->cd();
  //
  // // Call generator to save histograms to this file
  // GEO::BuildMCMapFromCurrentGEO();
  //
  // // Clean up
  // mapfile->Close();

  return;
}

// ---------------------------------------------------------
// Print End of Run Statement
// ---------------------------------------------------------
void Analysis::PrintEndOfRunStatement() {

    int curcount = fGeneratedEvents; //, int totalcount
    int totalcount = fRequiredEvents;

    // Get time taken so far
    long int curtime = time(0);
    long int prctime = curtime - fStartTime;

    std::cout << "************************************************************** " << std::endl;
    std::cout << "END: TotEvt : " << std::setfill(' ') << std::setw(6) << curcount << " Trg : " << std::setfill(' ') << std::setw(6) << fSavedEvents;
    G4double exptime = GetExposureTime();
    std::cout << " Exp : " << int(exptime) << "s";
    std::cout << " (" << std::setprecision(3) << exptime/60.0 << "min)";
    std::cout << " Rat (Hz) : " << std::setfill(' ') << std::setw(5) << std::setprecision(3) << float(fSavedEvents)/exptime;

    // Print N Events
    if (curcount > 0){
      if (fRunMode == kEventMode) {

        double remtime = double(totalcount * prctime / double(curcount));
        std::cout << " --> " << std::setprecision(3) << prctime/60.0 << "/"
                             << std::setprecision(3) << remtime/60.0 << "min" << std::endl;
        fLastCount = curcount;

      } else if (fRunMode == kTimeExposureMode) {

        double remtime = double(double(fRequiredExposure) * prctime / exptime);
        std::cout << " --> " << std::setprecision(3) << prctime/60.0 << "/"
                             << std::setprecision(3) << remtime/60.0 << "min" << std::endl;
        fLastCount = exptime;

      } else if (fRunMode == kTriggerMode) {

        double remtime = double(double(fRequiredTriggers) * prctime / double(fSavedEvents));
        std::cout << " --> " << std::setprecision(3) << prctime/60.0 << "/"
                             << std::setprecision(3) << remtime/60.0 << "min" << std::endl;
        fLastCount = fSavedEvents;

      }
    } else {
      std::cout << std::endl;
    }
}

} // - namespace Core
} // - namespace CRESTA
