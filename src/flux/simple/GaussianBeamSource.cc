#include "GaussianBeamSource.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Flux { // CRESTA Flux Sub Namespace
// ---------------------------------------------------------------------------

GaussianBeamSource::GaussianBeamSource()
    : G4VUserPrimaryGeneratorAction(),
      fParticleGun(0),
      fNThrows(0),
      fParticleTime(0)
{
    //    G4AutoLock lock(&myMutex);
    std::cout << "FLX: Building Isotropic Sphere Generator" << std::endl;

    // Setup Table
    DBTable table = DB::Get()->GetTable("GaussianBeamSource", "config");

    // Setup PDFs
    std::string pdffunc = "1.0"; //< Default Isotropic
    std::vector<double> pdfvars = std::vector<double>(0);

    std::vector<G4double> posvec = table.GetVecG4D("position");
    std::vector<G4double> fwhmposvec = table.GetVecG4D("fwhmposition");
    std::vector<G4double> dirvec = table.GetVecG4D("direction");

    fSourcePosition[0] = posvec[0];
    fSourcePosition[1] = posvec[1];
    fSourcePosition[2] = posvec[2];

    fSourcePosSpread[0] = fwhmposvec[0];
    fSourcePosSpread[1] = fwhmposvec[1];
    fSourcePosSpread[2] = fwhmposvec[2];

    fSourceDirection[0] = dirvec[0];
    fSourceDirection[1] = dirvec[1];
    fSourceDirection[2] = dirvec[2];

    // --> Particle Energy
    pdffunc = "1.0";
    pdfvars.clear();
    fMinEnergy = 1 * GeV;
    fMaxEnergy = 2 * GeV;
    if (table.Has("particle_energy_pdf")) {
        pdffunc = table.GetS("particle_energy_pdf");
        pdfvars = table.GetVecG4D("particle_energy_vars");
    }
    if (table.Has("particle_energy_min")) fMinEnergy = table.GetG4D("particle_energy_min");
    if (table.Has("particle_energy_max")) fMaxEnergy = table.GetG4D("particle_energy_max");
    fParticleEnergyPDF = new TF1("particle_energy_pdf", pdffunc.c_str(), fMinEnergy, fMaxEnergy);
    for (int i = 0; i < pdfvars.size(); i++) {
        fParticleEnergyPDF->SetParameter(i, pdfvars[i]);
    }
    fParticleEnergyPDF->SetRange(fMinEnergy, fMaxEnergy);

    // Setup Particle Gun
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();

    std::vector<std::string> particle_ids   = table.GetVecS("particle_ids");
    std::vector<double>      particle_probs = table.GetVecG4D("particle_probs");

    double probcount = 0.0;
    for (int i = 0; i < particle_ids.size(); i++) {

        std::string id = particle_ids[i];
        double prob = particle_probs[i];
        probcount += prob;
	G4ParticleDefinition* part = NULL;

	if (id.find("generic_ion_") != std::string::npos){

	  int Z = table.GetI(id + "_Z");
	  int A = table.GetI(id + "_A");
	  G4double excitEnergy = table.GetG4D(id + "_E");
	  part = NULL;
	  fParticleZ.push_back(Z);
	  fParticleA.push_back(A);
	  fParticleEX.push_back(excitEnergy);

	} else {

	  part = particleTable->FindParticle(id);
	  if (!part){
	    std::cout << "Could not find : " << id << " trying as int PDG" << std::endl;
	    part = particleTable->FindParticle(std::atoi(id.c_str()));
	  }
	  if (!part){
	    std::cout << "Could not find : " << id << " even as int" << std::endl;
	    throw;
	  }
	  fParticleZ.push_back(0);
	  fParticleA.push_back(0);
	  fParticleEX.push_back(0);

	}

        fParticleDefs.push_back( part );
        fParticlePDGs.push_back( 0 );
        fParticleProb.push_back(probcount);
    }

    for (int i = 0; i < fParticleProb.size(); i++) {
        fParticleProb[i] /= probcount;
    }

    // Setup Particle Gun
    std::cout << "FLX: --> Creating Particle Gun." << std::endl;
    G4int nofParticles = 1;
    fParticleGun  = new G4ParticleGun(nofParticles);

    // Get standard target and source boxes
    GetTargetBoxes();

    // Now setup the particle integrals and source/target boxes
    std::cout << "FLX: --> Complete." << std::endl;

    // Make a new processor
    Analysis::Get()->SetFluxProcessor(new GaussianBeamSourceProcessor(this));
}

GaussianBeamSource::~GaussianBeamSource()
{
    delete fParticleGun;
}

void GaussianBeamSource::SamplePositionAndDirection(G4ThreeVector& pos, G4ThreeVector& dir)
{

  // Change to use chosen direction, with some gaussian position smearing in X/Y/Z.
  // Position should be fixed point chosen by user.

  // Sample a position inside bounding box.
  pos[0] = fSourcePosition[0] + G4RandGauss::shoot(0.0,fSourcePosSpread[0]);
  pos[1] = fSourcePosition[1] + G4RandGauss::shoot(0.0,fSourcePosSpread[1]);
  pos[2] = fSourcePosition[2] + G4RandGauss::shoot(0.0,fSourcePosSpread[2]);

  dir = fSourceDirection;
  dir = (1.0/dir.mag()) * dir;

  return;
}



void GaussianBeamSource::SampleParticleType() {

    G4double r = G4UniformRand();
    for (int i = 0; i < fParticleProb.size(); i++) {
        if (r < fParticleProb[i]) {

	  if (fParticleDefs[i] == NULL){
	    fParticleDefs[i] = G4IonTable::GetIonTable()->GetIon(fParticleZ[i],
								 fParticleA[i],
								 fParticleEX[i]);

	    std::cout <<  "Constructing ION particle definition " << fParticleDefs[i]->GetPDGCharge() << std::endl;
	  }

	  fParticleGun->SetParticleDefinition(fParticleDefs[i]);
	  fParticlePDG = fParticlePDGs[i];
	  break;
        }
    }
}

std::vector<G4Box*> GaussianBeamSource::GetTargetBoxes() {

    // If presences of target boxes already been set then just return
    if (fCheckTargetBoxes) {
        return fTargetBoxes;
    }

    // If its not set but we have boxes return boxes
    if (fTargetBoxes.size() > 0) {
        fCheckTargetBoxes = true;
        return fTargetBoxes;
    }
    std::cout << "FLX: --> Creating Target boxes" << std::endl;

    // If none set then make it
    std::vector<DBTable> targetlinks = DB::Get()->GetTableGroup("FLUX");
    for (uint i = 0; i < targetlinks.size(); i++) {
        DBTable tbl = targetlinks[i];

        // Select tables with target box names
        std::string index = tbl.GetIndexName();
        if (index.find("target_box_") == std::string::npos) continue;

        // If it has position and size we can use it
        if (!tbl.Has("position") || !tbl.Has("size")) {
            std::cout << "Failed to find/create target box!" << std::endl;
            throw;
        }

        // Create objects
        std::vector<G4double> size = tbl.GetVecG4D("size");
        std::vector<G4double> pos  = tbl.GetVecG4D("position");

        G4Box* box_sol = new G4Box(index, 0.5 * size[0], 0.5 * size[1], 0.5 * size[2]);
        G4ThreeVector box_pos = G4ThreeVector(pos[0], pos[1], pos[2]);

        // Save Box
        fTargetBoxes.push_back(box_sol);
        fTargetBoxPositions.push_back(box_pos);

    }

    // Set flag and return
    fCheckTargetBoxes = true;
    return fTargetBoxes;
}



void GaussianBeamSource::Draw() {
}


std::vector<G4ThreeVector> GaussianBeamSource::GetTargetBoxPositions() {
    // If matching sizes its probs okay to return positions
    if (fTargetBoxes.size() == fTargetBoxPositions.size()) return fTargetBoxPositions;
    std::cout << "TargetBox Positions incorrect" << std::endl;
    throw;
}

void GaussianBeamSource::GeneratePrimaries(G4Event* anEvent) {

    if (!fCheckTargetBoxes) { GetTargetBoxes();  }


    // Sample the energy and particle type
    G4double E = 0.0;

    // Start of the rejection sampling of muon tracks
    bool good_event = false;
    uint throws = 0;
    G4ThreeVector direction = G4ThreeVector();
    G4ThreeVector position = G4ThreeVector();

    G4int num_target_boxes_hit = 0;
    G4double adjusted_rate = 1.0; //fFluxIntegrated*fArea/fSpeedUp;//< Adjust this rate if we are only sampling a smaller portion of the energy-angle PDF

    do {
        throws++;

        // Sample point and direction
	E = fParticleEnergyPDF->GetRandom();
	SampleParticleType();
        SamplePositionAndDirection(position, direction);

        // Keep track of global throws for integral
        fNThrows++;

        // If no target boxes defined all events are good
        if (fTargetBoxes.empty()) break;

        // If target boxes defined only save trajectories that hit at least one
        for (uint i = 0; i < fTargetBoxes.size(); i++) {

            G4double d = (fTargetBoxes.at(i))->DistanceToIn(
                             position - fTargetBoxPositions.at(i), direction);

            if (d != kInfinity) {
	      good_event = true;
	      break;
            }

            // Regardless of whether the event is accepted increment the time
            fParticleTime -= std::log(1 - G4UniformRand()) * (1.0 / adjusted_rate);


        }// End for



        if (throws >= 1E8) {
            std::cout << "Failed to find any good events in 1E6 tries!" << std::endl;
            throw;
        }

    } while (!good_event and throws < 1E8);

    fParticleDir = direction;
    fParticlePos = position;
    fParticleEnergy = E;

    fParticleGun->SetParticleEnergy(fParticleEnergy);
    fParticleGun->SetParticleTime(fParticleTime);
    fParticleGun->SetParticleMomentumDirection(fParticleDir);
    fParticleGun->SetParticlePosition(fParticlePos);
    fParticleGun->GeneratePrimaryVertex(anEvent);

    Draw();

    return;
}
//---------------------------------------------------------------------------------


//------------------------------------------------------------------
GaussianBeamSourceProcessor::GaussianBeamSourceProcessor(GaussianBeamSource* gen, bool autosave) :
    VFluxProcessor("iso"), fSave(autosave)
{
    fGenerator = gen;
}

bool GaussianBeamSourceProcessor::BeginOfRunAction(const G4Run* /*run*/) {

    std::string tableindex = "iso";
    std::cout << "FLX: Registering GaussianBeamSourceProcessor NTuples " << tableindex << std::endl;

    G4AnalysisManager* man = G4AnalysisManager::Instance();

    // Fill index energy
    fParticleTimeIndex   = man ->CreateNtupleDColumn(tableindex + "_t");
    fParticleEnergyIndex = man ->CreateNtupleDColumn(tableindex + "_E");
    fParticleDirXIndex   = man ->CreateNtupleDColumn(tableindex + "_dx");
    fParticleDirYIndex   = man ->CreateNtupleDColumn(tableindex + "_dy");
    fParticleDirZIndex   = man ->CreateNtupleDColumn(tableindex + "_dz");
    fParticlePosXIndex   = man ->CreateNtupleDColumn(tableindex + "_x");
    fParticlePosYIndex   = man ->CreateNtupleDColumn(tableindex + "_y");
    fParticlePosZIndex   = man ->CreateNtupleDColumn(tableindex + "_z");
    fParticlePDGIndex    = man ->CreateNtupleIColumn(tableindex + "_pdg");

    return true;
}

bool GaussianBeamSourceProcessor::ProcessEvent(const G4Event* /*event*/) {

    // Register Trigger State
    fHasInfo = true;
    fTime    = fGenerator->GetParticleTime();
    fEnergy  = fGenerator->GetParticleEnergy();

    // Set Ntuple to defaults

    if (fHasInfo) {
        G4AnalysisManager* man = G4AnalysisManager::Instance();
        man->FillNtupleDColumn(fParticleTimeIndex,   fGenerator->GetParticleTime());
        man->FillNtupleDColumn(fParticleEnergyIndex, fGenerator->GetParticleEnergy());
        man->FillNtupleDColumn(fParticleDirXIndex,   fGenerator->GetParticleDir().x() / MeV);
        man->FillNtupleDColumn(fParticleDirYIndex,   fGenerator->GetParticleDir().y() / MeV);
        man->FillNtupleDColumn(fParticleDirZIndex,   fGenerator->GetParticleDir().z() / MeV);
        man->FillNtupleDColumn(fParticlePosXIndex,   fGenerator->GetParticlePos().x() / m);
        man->FillNtupleDColumn(fParticlePosYIndex,   fGenerator->GetParticlePos().y() / m);
        man->FillNtupleDColumn(fParticlePosZIndex,   fGenerator->GetParticlePos().z() / m);
        man->FillNtupleIColumn(fParticlePDGIndex ,   fGenerator->GetParticlePDG());
        return true;
    } else {
        G4AnalysisManager* man = G4AnalysisManager::Instance();
        man->FillNtupleDColumn(fParticleTimeIndex, -999.);
        man->FillNtupleDColumn(fParticleEnergyIndex, -999.);
        man->FillNtupleDColumn(fParticleDirXIndex, -999.);
        man->FillNtupleDColumn(fParticleDirYIndex, -999.);
        man->FillNtupleDColumn(fParticleDirZIndex, -999.);
        man->FillNtupleDColumn(fParticlePosXIndex, -999.);
        man->FillNtupleDColumn(fParticlePosYIndex, -999.);
        man->FillNtupleDColumn(fParticlePosZIndex, -999.);
        man->FillNtupleIColumn(fParticlePDGIndex,  -999 );
        return false;
    }
    return true;
}

G4double GaussianBeamSourceProcessor::GetExposureTime() {
    return fGenerator->GetParticleTime();
}

// ---------------------------------------------------------------------------
} // - namespace Flux
} // - namespace CRESTA
