#ifndef __CRESTA_PrimaryGeneratorFactory_HH__
#define __CRESTA_PrimaryGeneratorFactory_HH__
#include "DBPackage.hh"
#include "CorePackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Flux { // CRESTA Flux Sub Namespace
// ---------------------------------------------------------------------------

/// Flux Factory function to load generators from table
namespace FluxFactory {
  G4VUserPrimaryGeneratorAction* LoadFluxGenerator();
  G4VUserPrimaryGeneratorAction* LoadFluxGenerator(DBTable table);
}

// ---------------------------------------------------------------------------
} // - namespace Flux
} // - namespace CRESTA
#endif
