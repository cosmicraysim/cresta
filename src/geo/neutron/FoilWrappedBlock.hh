#ifndef __FoilWrappedBlock_HH__
#define __FoilWrappedBlock_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

#include "geo/simple/GeoSolid.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

class FoilWrappedBlock : public GeoSolid {
public:

  /// Can only be constructed from a table
  FoilWrappedBlock(DBTable table);

  /// Builds the geometry from some template DB file
  void Construct(DBTable table);

  // Return each sensitive
  inline G4VSensitiveDetector* GetCoatingSensitive(){ return fCoatingSensitive; };
  inline G4VSensitiveDetector* GetBlockSensitive(){ return fBlockSensitive; };

private:
  G4VSensitiveDetector* fCoatingSensitive;
  G4VSensitiveDetector* fBlockSensitive;
};

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
#endif
