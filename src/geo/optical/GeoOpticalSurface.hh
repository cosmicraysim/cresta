#ifndef __GEO_OPTICALSURFACE_HH__
#define __GEO_OPTICALSURFACE_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "geo/simple/GeoSolid.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

class GeoOpticalSurface : public GeoSolid {
public:
  /// Create a GeoOpticalSurface from a table
  GeoOpticalSurface(DBTable table);

  /// Construct for this geobox
  void Construct(DBTable table);
};

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
#endif
