#ifndef __GEOUTILS_HH__
#define __GEOUTILS_HH__
#include <iostream>
#include "ROOTHeaders.hh"
#include "G4Headers.hh"
#include "DBPackage.hh"

// namespace
namespace CRESTA {
namespace Geometry {
namespace GEOUtils {

/// Get a solid from the Geant4 solid store via name
G4VSolid* GetSolidFromStore(std::string name, bool verbose = true);
/// Get a G4Box from the Geant4 solid store via name
G4Box* GetBoxFromStore(std::string name, bool verbose = true);
/// Get a logical volume from the Geant4 logical volume store via name
G4LogicalVolume* GetLogicalFromStore(std::string name, bool verbose = true);
/// Get a mother logical volume from the store via a table
G4LogicalVolume* GetMotherLogicalFromStore(DBTable tb, bool verbose = true);
/// Get physical volume from G4 store.
G4VPhysicalVolume* GetPhysicalFromStore(std::string name, bool verbose = true);

/// Build a set of MC TH3D histograms from current GEO
void BuildMCMapFromCurrentGEO();
} // - namespace GEOUtils

} // - namespace GEO
} // - namespace CRESTA

#endif
