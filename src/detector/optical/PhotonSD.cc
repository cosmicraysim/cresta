#include "PhotonSD.hh"

#include "PhotonHit.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

PhotonSD::PhotonSD(DBTable tbl):
  VDetector(tbl.GetIndexName(), "photonsd"), fHCID(-1)
{
  std::cout << "DET: Creating new " << GetType()
            << " detector : " << GetID() << std::endl;

  // Set initial state
  ResetState();

  // By default also include the auto processor
  Analysis::Get()->RegisterProcessor(new PhotonProcessor(this));

  collectionName.push_back(GetID());

}


void PhotonSD::ResetState() {
  VDetector::ResetState();
}

G4bool PhotonSD::ProcessHits(G4Step* step, G4TouchableHistory* /*touch*/) {
  return false;
}

G4TrackStatus PhotonSD::ManuallyProcessHits(const G4Step* step, G4TouchableHistory* touch) {

  // Require a photon
  if (step->GetTrack()->GetDefinition()
      != G4OpticalPhoton::OpticalPhotonDefinition()) return step->GetTrack()->GetTrackStatus();

  PhotonHit* hit = new PhotonHit();
  hit->SetTime(step->GetPostStepPoint()->GetGlobalTime());
  hit->SetEnergy(step->GetTrack()->GetTotalEnergy());
  hit->SetPosition(step->GetPostStepPoint()->GetPosition());
  hit->SetDirection(step->GetTrack()->GetMomentumDirection());
  fHitsCollection->insert(hit);

  step->GetTrack()->SetTrackStatus(fKillTrackAndSecondaries);

  return fKillTrackAndSecondaries;
};

void PhotonSD::Initialize(G4HCofThisEvent* hce)
{
  fHitsCollection = new PhotonHitsCollection(SensitiveDetectorName, collectionName[0]);
  if (fHCID < 0) fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(fHitsCollection);
  hce->AddHitsCollection(fHCID, fHitsCollection);
}


//------------------------------------------------------------------
PhotonProcessor::PhotonProcessor(PhotonSD* trkr, bool autosave) :
  VProcessor(trkr->GetID()), fHCID(-1)
{
  fDetector = trkr;
}

bool PhotonProcessor::BeginOfRunAction(const G4Run* /*run*/) {

// Each of these should be vectors
  std::string tableindex = GetID();
  G4AnalysisManager* man = G4AnalysisManager::Instance();
  man->CreateNtupleDColumn(tableindex + "_E", fPhotonE);
  man->CreateNtupleDColumn(tableindex + "_t", fPhotonT);
  man->CreateNtupleDColumn(tableindex + "_x", fPhotonX);
  man->CreateNtupleDColumn(tableindex + "_y", fPhotonY);
  man->CreateNtupleDColumn(tableindex + "_z", fPhotonZ);
  man->CreateNtupleDColumn(tableindex + "_vx", fPhotonVX);
  man->CreateNtupleDColumn(tableindex + "_vy", fPhotonVY);
  man->CreateNtupleDColumn(tableindex + "_vz", fPhotonVZ);
  Reset();

  return true;
}

bool PhotonProcessor::ProcessEvent(const G4Event* event) {

  if (fHCID < 0) fHCID = fDetector->GetHCID();
  PhotonHitsCollection* hc = static_cast<PhotonHitsCollection*> (event->GetHCofThisEvent()->GetHC(fHCID));
  if (!hc) return false;

  int nhits = (int) (hc)->GetSize();

  if (nhits < 1) {
    return false;
  }
  fPhotonE.clear();
  fPhotonT.clear();
  G4double firsttime = 0;
  std::cout << "Collected " << nhits << " photons." << std::endl;
  for (int i = 0; i < nhits; i++) {
    PhotonHit* hit = ( *(hc) )[i];
    //    if (firsttime == 0 or hit->GetTime() < firsttime) firsttime = hit->GetTime();
    //	std::cout << "Got Photon hit at " << hit->GetTime() << std::endl;
    fPhotonE.push_back(hit->GetEnergy()/MeV);
    fPhotonT.push_back(hit->GetTime()/ns);
    fPhotonX.push_back(hit->GetPosition()[0]/m);
    fPhotonY.push_back(hit->GetPosition()[1]/m);
    fPhotonZ.push_back(hit->GetPosition()[2]/m);
    fPhotonVX.push_back(hit->GetDirection()[0]/MeV);
    fPhotonVY.push_back(hit->GetDirection()[1]/MeV);
    fPhotonVZ.push_back(hit->GetDirection()[2]/MeV);
  }

  fHasInfo = true;
  return fHasInfo;
}

  void PhotonProcessor::Reset(){
    fPhotonE.clear();
    fPhotonT.clear();
    fPhotonX.clear();
    fPhotonY.clear();
    fPhotonZ.clear();
    fPhotonVX.clear();
    fPhotonVY.clear();
    fPhotonVZ.clear();
  }

// ---------------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
