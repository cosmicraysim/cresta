#include "TrueNeutronTracker.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

TrueNeutronTracker::TrueNeutronTracker(DBTable tbl):
  VDetector(tbl.GetIndexName(), "trueproton")
{
  std::cout << "DET: Creating new " << GetType()
            << " detector : " << GetID() << std::endl;

  // Set initial state
  ResetState();
  fUpward = false;
  if (tbl.Has("onlyupward")) fUpward = tbl.GetI("onlyupward") > 0;

  // By default also include the auto processor
  if (!tbl.Has("processor") or tbl.GetI("processor") > 0) {
    Analysis::Get()->RegisterProcessor(new TrueNeutronProcessor(this));
  }
}

TrueNeutronTracker::TrueNeutronTracker(std::string name, std::string id, bool autoprocess, bool autosave):
  VDetector(name, id)
{
  std::cout << "DET: Creating new " << GetType()
            << " detector : " << GetID() << std::endl;

  // Set initial state
  ResetState();

  // By default also include the auto processor
  if (autoprocess) {
    Analysis::Get()->RegisterProcessor(new TrueNeutronProcessor(this, autosave));
  }
}

void TrueNeutronTracker::ResetState() {
  VDetector::ResetState();
  fNeutronTime = 0.0;
  fNeutronMom = G4ThreeVector();
  fNeutronPos = G4ThreeVector();
  fNeutronKE  = 0.0;
  fNeutronPDG = -999;
  fTotEDep = 0.0;
  fNNeutronHits = 0;
}

G4bool TrueNeutronTracker::ProcessHits(G4Step* step, G4TouchableHistory* /*touch*/) {

  // // Don't save tracks if no energy left in the detector
  // G4double edep = step->GetTotalEnergyDeposit();
  // fTotEDep += edep;
  // if (edep <= 0.) return false;
  if (fNNeutronHits > 0) return false;

  // Get only protons
  G4Track* track = step->GetTrack();
  int steppdg = track->GetParticleDefinition()->GetPDGEncoding();
  G4double pz = step->GetPostStepPoint()->GetMomentum()[2];
  //std::cout << " PZ : " << pz << " " << fUpward << std::endl;
  if (pz < 0.0 and fUpward == true) {
    //std::cout << "Skipping downward going neutron" << std::endl;
    return false;
  }

  //  if (steppdg != 13 and steppdg != -13) std::cout << "Got Track : " << steppdg << std::endl;
  bool is_neutron = (steppdg == 2112);
  if (!is_neutron) return false;

  // Get the step+1 inside the detector
  G4StepPoint* steppoint = step->GetPostStepPoint();
  G4double steptime = steppoint->GetGlobalTime();
  G4ThreeVector steppos = steppoint->GetPosition();
  G4ThreeVector stepmom = track->GetMomentum();
  G4double kineticenergy = track->GetKineticEnergy();
  
  fNeutronPos  *= fNNeutronHits;
  fNeutronMom  *= fNNeutronHits;
  fNeutronTime *= fNNeutronHits;
  fNeutronPDG  *= fNNeutronHits;
  fNeutronKE   *= fNNeutronHits;
  
  fNeutronPos  += steppos;
  fNeutronMom  += stepmom;
  fNeutronTime += steptime;
  fNeutronPDG  += steppdg;
  fNeutronKE   += kineticenergy;
  fNNeutronHits += 1;

  fNeutronPos  /= fNNeutronHits;
  fNeutronMom  /= fNNeutronHits;
  fNeutronTime /= fNNeutronHits;
  fNeutronPDG  /= fNNeutronHits;
  fNeutronKE   /= fNNeutronHits;
  fTotEDep = stepmom.mag();
  track->SetTrackStatus(fStopAndKill);

  return true;
}


//------------------------------------------------------------------
TrueNeutronProcessor::TrueNeutronProcessor(TrueNeutronTracker* trkr, bool autosave) :
  VProcessor(trkr->GetID()), fSave(autosave)
{
  fTracker = trkr;
}

bool TrueNeutronProcessor::BeginOfRunAction(const G4Run* /*run*/) {

  std::string tableindex = GetID();
  std::cout << "DET: Registering TrueNeutronProcessor NTuples " << tableindex << std::endl;

  G4AnalysisManager* man = G4AnalysisManager::Instance();

  // Fill index energy
  fNeutronTimeIndex = man ->CreateNtupleDColumn(tableindex + "_t");
  fNeutronMomXIndex = man ->CreateNtupleDColumn(tableindex + "_px");
  fNeutronMomYIndex = man ->CreateNtupleDColumn(tableindex + "_py");
  fNeutronMomZIndex = man ->CreateNtupleDColumn(tableindex + "_pz");
  fNeutronPosXIndex = man ->CreateNtupleDColumn(tableindex + "_x");
  fNeutronPosYIndex = man ->CreateNtupleDColumn(tableindex + "_y");
  fNeutronPosZIndex = man ->CreateNtupleDColumn(tableindex + "_z");
  fNeutronKinEIndex = man ->CreateNtupleDColumn(tableindex + "_ke");
  fNeutronPDGIndex  = man ->CreateNtupleIColumn(tableindex + "_pdg");

  return true;
}

bool TrueNeutronProcessor::ProcessEvent(const G4Event* /*event*/) {

  // Register Trigger State
  fHasInfo = fTracker->GetNeutronPDG() != -999;
  fTime    = fTracker->GetNeutronTime();
  fEnergy  = fTracker->GetTotEDep();

  if (fHasInfo) {
    // Fill neutron vectors
    G4AnalysisManager* man = G4AnalysisManager::Instance();
    //    std::cout << "Filling Neutron Energy : " << fTracker->GetNeutronMom().z() << " " << GetID() << std::endl;

    man->FillNtupleDColumn(fNeutronTimeIndex, fTracker->GetNeutronTime());
    man->FillNtupleDColumn(fNeutronMomXIndex, fTracker->GetNeutronMom().x() / MeV);
    man->FillNtupleDColumn(fNeutronMomYIndex, fTracker->GetNeutronMom().y() / MeV);
    man->FillNtupleDColumn(fNeutronMomZIndex, fTracker->GetNeutronMom().z() / MeV);
    man->FillNtupleDColumn(fNeutronPosXIndex, fTracker->GetNeutronPos().x() / m);
    man->FillNtupleDColumn(fNeutronPosYIndex, fTracker->GetNeutronPos().y() / m);
    man->FillNtupleDColumn(fNeutronPosZIndex, fTracker->GetNeutronPos().z() / m);
    man->FillNtupleIColumn(fNeutronPDGIndex , fTracker->GetNeutronPDG());
    man->FillNtupleDColumn(fNeutronKinEIndex, fTracker->GetNeutronKE());

    //    TrueNeutronProcessor::DrawEvent();

    return true;
  } else {
    // Set Ntuple to defaults
    G4AnalysisManager* man = G4AnalysisManager::Instance();
    man->FillNtupleDColumn(fNeutronTimeIndex, -999.);
    man->FillNtupleDColumn(fNeutronMomXIndex, -999.);
    man->FillNtupleDColumn(fNeutronMomYIndex, -999.);
    man->FillNtupleDColumn(fNeutronMomZIndex, -999.);
    man->FillNtupleDColumn(fNeutronPosXIndex, -999.);
    man->FillNtupleDColumn(fNeutronPosYIndex, -999.);
    man->FillNtupleDColumn(fNeutronPosZIndex, -999.);
    man->FillNtupleDColumn(fNeutronKinEIndex, -999.);
    man->FillNtupleIColumn(fNeutronPDGIndex,  fTracker->GetNeutronPDG() );
    return false;
  }
}

void TrueNeutronProcessor::DrawEvent(){
}

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
