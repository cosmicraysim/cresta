#include "TrueMuonSaveAndKill.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

TrueMuonSaveAndKill::TrueMuonSaveAndKill(DBTable tbl):
  VDetector(tbl.GetIndexName(), "truemuonsaveandkill")
{
  std::cout << "DET: Creating new " << GetType()
            << " detector : " << GetID() << std::endl;

  // Set initial state
  ResetState();

  // By default also include the auto processor
  if (!tbl.Has("processor") or tbl.GetI("processor") > 0) {
    Analysis::Get()->RegisterProcessor(new TrueMuonSaveAndKillProcessor(this));
  }
}

TrueMuonSaveAndKill::TrueMuonSaveAndKill(std::string name, std::string id, bool autoprocess, bool autosave):
  VDetector(name, id)
{
  std::cout << "DET: Creating new " << GetType()
            << " detector : " << GetID() << std::endl;

  // Set initial state
  ResetState();

  // By default also include the auto processor
  if (autoprocess) {
    Analysis::Get()->RegisterProcessor(new TrueMuonSaveAndKillProcessor(this, autosave));
  }
}

void TrueMuonSaveAndKill::ResetState() {
  VDetector::ResetState();
  fMuonTime = 0.0;
  fMuonMom = G4ThreeVector();
  fMuonPos = G4ThreeVector();
  fMuonPDG = -999;
  fTotEDep = 0.0;
  fNMuonHits = 0;
  fHasMuon = false;
}

G4bool TrueMuonSaveAndKill::ProcessHits(G4Step* step, G4TouchableHistory* /*touch*/) {

  // Get only muons
  G4Track* track = step->GetTrack();
  int steppdg = track->GetParticleDefinition()->GetPDGEncoding();
  bool is_muon = ((steppdg == 13) || (steppdg == -13));
  if (!is_muon) return true;

  // Get the step+1 inside the detector
  G4StepPoint* steppoint = step->GetPreStepPoint();
  G4double steptime = steppoint->GetGlobalTime();
  G4ThreeVector steppos = steppoint->GetPosition();
  G4ThreeVector stepmom = track->GetMomentum();
  G4ThreeVector postpos = step->GetPostStepPoint()->GetPosition();

  // Very inefficient lazy averaging
  if (!fHasMuon){
    fMuonPos  = steppos;
    fMuonMom  = stepmom;
    fMuonTime = steptime;
    fMuonPDG  = steppdg;
    fHasMuon = true;
  }

  track->SetTrackStatus(fKillTrackAndSecondaries);

  return true;
}


//------------------------------------------------------------------
TrueMuonSaveAndKillProcessor::TrueMuonSaveAndKillProcessor(TrueMuonSaveAndKill* trkr, bool autosave) :
  VProcessor(trkr->GetID()), fSave(autosave)
{
  fKiller = trkr;
}

bool TrueMuonSaveAndKillProcessor::BeginOfRunAction(const G4Run* /*run*/) {

  std::string tableindex = GetID();
  std::cout << "DET: Registering TrueMuonSaveAndKillProcessor NTuples " << tableindex << std::endl;

  G4AnalysisManager* man = G4AnalysisManager::Instance();

  // Fill index energy
  fMuonTimeIndex = man ->CreateNtupleFColumn(tableindex + "_t");
  fMuonMomXIndex = man ->CreateNtupleFColumn(tableindex + "_px");
  fMuonMomYIndex = man ->CreateNtupleFColumn(tableindex + "_py");
  fMuonMomZIndex = man ->CreateNtupleFColumn(tableindex + "_pz");
  fMuonPosXIndex = man ->CreateNtupleFColumn(tableindex + "_x");
  fMuonPosYIndex = man ->CreateNtupleFColumn(tableindex + "_y");
  fMuonPosZIndex = man ->CreateNtupleFColumn(tableindex + "_z");
  fMuonPDGIndex  = man ->CreateNtupleIColumn(tableindex + "_pdg");

  return true;
}

bool TrueMuonSaveAndKillProcessor::ProcessEvent(const G4Event* /*event*/) {

  // Register Trigger State
  fHasInfo = fKiller->GetMuonPDG() != -999;
  fTime    = fKiller->GetMuonTime();
  fEnergy  = fKiller->GetTotEDep();

  if (fHasInfo) {
    // Fill muon vectors
    G4AnalysisManager* man = G4AnalysisManager::Instance();
    man->FillNtupleFColumn(fMuonTimeIndex, fKiller->GetMuonTime() / s);
    man->FillNtupleFColumn(fMuonMomXIndex, fKiller->GetMuonMom().x() / MeV);
    man->FillNtupleFColumn(fMuonMomYIndex, fKiller->GetMuonMom().y() / MeV);
    man->FillNtupleFColumn(fMuonMomZIndex, fKiller->GetMuonMom().z() / MeV);
    man->FillNtupleFColumn(fMuonPosXIndex, fKiller->GetMuonPos().x() / m);
    man->FillNtupleFColumn(fMuonPosYIndex, fKiller->GetMuonPos().y() / m);
    man->FillNtupleFColumn(fMuonPosZIndex, fKiller->GetMuonPos().z() / m);
    man->FillNtupleIColumn(fMuonPDGIndex , fKiller->GetMuonPDG());

    //    TrueMuonSaveAndKillProcessor::DrawEvent();

    return true;
  } else {
    // Set Ntuple to defaults
    G4AnalysisManager* man = G4AnalysisManager::Instance();
    man->FillNtupleFColumn(fMuonTimeIndex, -999.);
    man->FillNtupleFColumn(fMuonMomXIndex, -999.);
    man->FillNtupleFColumn(fMuonMomYIndex, -999.);
    man->FillNtupleFColumn(fMuonMomZIndex, -999.);
    man->FillNtupleFColumn(fMuonPosXIndex, -999.);
    man->FillNtupleFColumn(fMuonPosYIndex, -999.);
    man->FillNtupleFColumn(fMuonPosZIndex, -999.);
    man->FillNtupleIColumn(fMuonPDGIndex,  fKiller->GetMuonPDG() );
    return false;
  }
}

void TrueMuonSaveAndKillProcessor::DrawEvent(){
  // Draw Track if in interactive
  G4VVisManager* pVVisManager = Analysis::Get()->GetVisManager();
  if (pVVisManager)
  {
    G4ThreeVector muonmom = fKiller->GetMuonMom();
    G4ThreeVector muonpos = fKiller->GetMuonPos();

    G4Polyline polyline;
    polyline.push_back( muonpos + 4.0 * m * muonmom );
    polyline.push_back( muonpos - 4.0 * m * muonmom );

    G4Colour colour(0., 0., 1.);
    G4VisAttributes attribs(colour);
    polyline.SetVisAttributes(attribs);

    pVVisManager->Draw(polyline);
  }
}

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
