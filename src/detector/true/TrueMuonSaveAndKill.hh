#ifndef __TrueMuonSaveAndKill_HH__
#define __TrueMuonSaveAndKill_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

/// \brief TrueMuonSaveAndKill Detector
///
/// Measures the  true muon trajectory the first muon entering a volume.
/// ALL muons entering the volume are then killed.
class TrueMuonSaveAndKill : public VDetector {
public:

  /// Main constructor from a database table
  TrueMuonSaveAndKill(DBTable table);
  /// Simple C++ constructor
  TrueMuonSaveAndKill(std::string name, std::string id, bool autoprocess=true, bool autosave=true);
  /// Destructor
  ~TrueMuonSaveAndKill(){};

  /// Main processing. Looks for highest momentum track.
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /// Reset all information
  void ResetState();

  // Getter Functions
  inline G4double      GetMuonTime(){ return fMuonTime; };
  inline G4ThreeVector  GetMuonMom(){ return fMuonMom;  };
  inline G4ThreeVector  GetMuonPos(){ return fMuonPos;  };
  inline G4double       GetTotEDep(){ return fTotEDep;  };
  inline int            GetMuonPDG(){ return fMuonPDG;  };

protected:

  G4double      fMuonTime; ///< HM Muon Step Time
  G4ThreeVector fMuonMom;  ///< HM Muon Mom Vector
  G4ThreeVector fMuonPos;  ///< HM Muon Pos Vector
  G4double      fTotEDep;  ///< Total Energy Deposited this event
  int           fMuonPDG;  ///< Muon PDG Code
  int           fNMuonHits; //< Number of deposits
  bool fHasMuon;
};
// ---------------------------------------------------------------------------

/// \brief TrueMuonSaveAndKill Processor
///
/// Saves the measured data from a TrueMuonSaveAndKill Detector to disk.
class TrueMuonSaveAndKillProcessor : public VProcessor {
public:

  /// Processor can only be created with an associated
  /// tracker object.
  TrueMuonSaveAndKillProcessor(TrueMuonSaveAndKill* trkr, bool autosave=true);
  /// Destructor
  ~TrueMuonSaveAndKillProcessor(){};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Process the information the tracker recieved for this event
  bool ProcessEvent(const G4Event* event);

  /// Draw function
  void DrawEvent();

protected:

  TrueMuonSaveAndKill* fKiller; ///< Pointer to associated tracker SD

  bool fSave; ///< Flag to save event info automatically

  int fMuonTimeIndex; ///< Time Ntuple Index
  int fMuonMomXIndex; ///< MomX Ntuple Index
  int fMuonMomYIndex; ///< MomY Ntuple Index
  int fMuonMomZIndex; ///< MomZ Ntuple Index
  int fMuonPosXIndex; ///< PosX Ntuple Index
  int fMuonPosYIndex; ///< PosY Ntuple Index
  int fMuonPosZIndex; ///< PosZ Ntuple Index
  int fMuonPDGIndex;  ///< MPDG Ntuple Index

};

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
#endif
