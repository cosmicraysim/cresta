#ifndef __FLUX_PACKAGE_HH__
#define __FLUX_PACKAGE_HH__
// HACK way to hide DB includes in later packages and have the user not have to
// worry about....
#include "flux/FluxFactory.hh"
#include "flux/cry/CRYPrimaryGenerator.hh"
#include "flux/radiography/MuonInputTree.hh"
#include "flux/simple/IsotropicSphereSource.hh"
#include "flux/simple/PrimaryGeneratorROOT.hh"
using namespace CRESTA::Flux;
#endif
