#ifndef __GEOMETRY_PACKAGE_HH__
#define __GEOMETRY_PACKAGE_HH__
// HACK way to hide DB includes in later packages and have the user not have to
// worry about....
#include "geo/GeoFactory.hh"
#include "geo/GeoUtils.hh"
#include "geo/simple/GeoBox.hh"
#include "geo/simple/GeoSolid.hh"
#include "geo/simple/GeoCone.hh"
#include "geo/simple/GeoCutTubs.hh"
#include "geo/simple/GeoEllipticalTube.hh"
#include "geo/simple/GeoPara.hh"
#include "geo/simple/GeoTubs.hh"
using namespace CRESTA::Geometry;
#endif
