#ifndef __DETECTOR_PACKAGE_HH__
#define __DETECTOR_PACKAGE_HH__
// HACK way to hide DB includes in later packages and have the user not have to
// worry about....
#include "detector/DetectorFactory.hh"
#include "detector/true/TrueMuonTracker.hh"
#include "detector/true/TrueInTracker.hh"
#include "detector/true/TrueParticleKiller.hh"
#include "detector/true/TrueMuonSaveAndKill.hh"
#include "detector/simple/SimpleScintillatorSD.hh"
#include "detector/simple/GridScintillatorSD.hh"
#include "detector/drift/LongDriftSD.hh"
#include "detector/tomography/TargetMuonInOutTracker.hh"
#include "detector/neutron/NeutronSD.hh"
#include "detector/optical/PhotonSD.hh"

using namespace CRESTA::Detector;
#endif
