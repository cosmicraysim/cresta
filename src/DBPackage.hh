#ifndef __DB_PACKAGE_HH__
#define __DB_PACKAGE_HH__
// HACK way to hide DB includes in later packages and have the user not have to
// worry about....
#include "db/DB.hh"
#include "db/DBEvaluator.hh"
#include "db/DBTable.hh"
#include "db/json.hh"
#include "db/ReadFile.hh"
using namespace CRESTA::DataBase;
#endif
