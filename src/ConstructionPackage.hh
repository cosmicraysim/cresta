#ifndef __CONSTRUCTION_PACKAGE_HH__
#define __CONSTRUCTION_PACKAGE_HH__
// HACK way to hide CORE includes in later packages and have the user not have to
// worry about it....
#include "construction/ActionConstruction.hh"
#include "construction/PhysicsConstruction.hh"
#include "construction/FluxConstruction.hh"
#include "construction/WorldConstruction.hh"
#include "construction/MaterialsFactory.hh"
using namespace CRESTA::Construction;

#endif
