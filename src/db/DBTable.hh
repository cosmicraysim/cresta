// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with NUISANCE.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef __DBTABLE_HH__
#define __DBTABLE_HH__
#include "SystemHeaders.hh"
#include "G4Headers.hh"

#include "DBEvaluator.hh"
#include "json.hh"
#include "ReadFile.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace DataBase { // CRESTA Database Sub Namespace
// ---------------------------------------------------------------------------

/// \class DBTable
/// \brief Database Table class.
///
/// Database Table Class. A wrapper around the json table reader
class DBTable {
public:

  /// Empty constructor
  DBTable();
  /// Constructor from other JSON
  DBTable(json::Value v);
  /// Constructor setting name and index
  DBTable(std::string name, std::string ind);
  /// Copy Constructor
  DBTable(const DBTable &right);
  /// Copy operator
  const DBTable& operator=(const DBTable &right);
  /// Destructor, wipes JSON info
  ~DBTable();
  /// Creates a cloned object
  DBTable Clone();

  // Setting functions, change or add stuff to JSON
  void Set(std::string field, int i);                       ///< Set integer value
  void Set(std::string field, double i);                    ///< Set integer value
  void Set(std::string field, std::string s);               ///< Set std::string value
  void Set(std::string field, std::vector<int> iv);         ///< Set vector int
  void Set(std::string field, std::vector<double> dv);      ///< Set vector double
  void Set(std::string field, std::vector<std::string> sv); ///< Set vector double
  void Set(std::string field, G4ThreeVector v); ///< Set from 3D vector

  void SetTableName(std::string n); ///< Special table ID set function
  void SetIndexName(std::string n); ///< Special index ID set function

  void Prefix(std::string name, std::string pref); ///< Prefix a string entry with another string value

  /// Print out details in the table
  void Print();

  std::string GetTableName(); ///< Special name request
  std::string GetIndexName(); ///< Special index request

  // Request functions
  bool Has(std::string field); ///< Return if field is in this table
  std::vector<std::string> GetFields(); ///< Return list of all fields present
  json::Type GetType(std::string field); ///< Return JSON Type Member

  bool GetB(std::string name); ///< Get Value from table as BOOL
  int GetI(std::string name); ///< Get Value from table as I
  float GetF(std::string name); ///< Get Value from table as F
  double GetD(std::string name); ///< Get Value from table as D
  std::string GetS(std::string name, bool noeval=false); ///< Get String

  /// Get Vector Bool 'name: ["val1","val2",...]'
  std::vector<bool>        GetVecB(std::string name);
  std::vector<int>         GetVecI(std::string name); ///< Get Vector Integer
  std::vector<float>       GetVecF(std::string name); ///< Get Vector Float
  std::vector<double>      GetVecD(std::string name); ///< Get Vector Double
  std::vector<std::string> GetVecS(std::string name); ///< Get Vector String

  G4double Evaluate(std::string value); ///< Take a string and substitude other variables
  G4double GetUnits(std::string field); ///< Get G4double units to go with value
  G4double GetG4D(std::string name);    ///< Get G4double object with units
  std::vector<G4double>    GetVecG4D(std::string name); ///< Get vector of G4double objects

  G4double GetLengthUnits(std::string field); ///< Ask for units for a given var.
  G4double GetAngleUnits(std::string field); ///< Ask for units for a given var.
  G4double GetMassUnits(std::string field); ///< Ask for units for a given var.
  G4double GetEnergyUnits(std::string field); ///< Ask for units for a given var.

  G4ThreeVector Get3DPosition(std::string field); ///< Get a vector and push into G43.
  G4ThreeVector Get3DLength(std::string field); ///< Get a vector and push into G43.
  G4ThreeVector Get3DRotation(std::string field); ///< Get a vector and push into G43.

  G4double      GetLength(std::string field); ///< Auto scale by length unit.
  G4double      GetAngle(std::string field); ///< Auto scale by angle unit.
  G4double      GetMass(std::string field); ///< Auto scale by mass unit.
  G4double      GetEnergy(std::string field); ///< Auto scale by energy unit.

  /// Return the actual table objecy
  inline json::Value GetTable() { return table; };

  /// Deprecated REQUESTS
  DBTable* GetT(std::string name);

  /// Get the original JSON used to make this table
  inline json::Value GetJSON() {return table;};

  /// Override values in this table with another table.
  void UpdateFields(DBTable* overrides);

protected:

  void ThrowNotFoundError(std::string s); ///< Easier function call to throw errors
  json::Value table; ///< Actual JSON value we are wrapping around
};


// ---------------------------------------------------------------------------
// Parser class for static calls
class DBJSONParser
{
public:
  /** Returns a list of all tables found in JSON text file @p filename. */
  static std::vector<DBTable *> parse(const std::string &filename);

  /** Returns a list of all tables found in JSON string. */
  static std::vector<DBTable *> parseString(const std::string &data);

  /** Converts a JSON document to a RATDB table */
  static DBTable *convertTable(json::Value &jsonDoc);

    /** Returns a list of all tables found in JSON text file @p filename. */
  static std::vector<DBTable> parsevals(const std::string &filename);

  /** Returns a list of all tables found in JSON string. */
  static std::vector<DBTable> parseStringvals(const std::string &data);

  /** Converts a JSON document to a RATDB table */
  static DBTable convertTablevals(json::Value &jsonDoc);
};
// ---------------------------------------------------------------------------



// ---------------------------------------------------------------------------
/** Exception: Field not found in DB */
class DBNotFoundError {
public:
  /** Create new exception.
   *
   *  @param _table  Name of table in which field could not be found
   *  @param _index  Index of table in which field could not be found
   *  @param _field  Name of field which could not be found
   */
  DBNotFoundError (const std::string &_table, const std::string &_index,
                   const std::string &_field) :
    table(_table), index(_index), field(_field) {
    std::cout << "DBNotFoundError : " << table << " : " << index << " : " << field << std::endl;
  };

  /** Compare if @p other has the same table, index and field as @p this. */
  bool operator== (const DBNotFoundError &other) const {
    return table == other.table && index == other.index && field == other.field;
  }

  std::string table;
  std::string index;
  std::string field;
};

// ---------------------------------------------------------------------------
} // - namespace DataBase
} // - namespace CRESTA
#endif
