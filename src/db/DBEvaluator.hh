// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with NUISANCE.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
// This is a modification of G4 Evalutor to use global params.
#ifndef _CRESTA_DBEVALUATOR_HH_
#define _CRESTA_DBEVALUATOR_HH_
#include "SystemHeaders.hh"
#include "G4Headers.hh"

#include "DBTable.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace DataBase { // CRESTA Database Sub Namespace
// ---------------------------------------------------------------------------

/// \class DBEvaluator
/// \brief Database Evaluator Class.
///
/// Uses all global variables defined in VARIABLE tables
/// to evaluate constants and arithmetic in the DBTable
/// objects.
class DBEvaluator
{
 public:
   DBEvaluator(); ///< Constructor
   ~DBEvaluator(); ///< Destructor

   void Clear(); ///< Clear Constants.
   void DefineConstant(const G4String&, G4double); ///< Set a constant.

   /// Define a matrix. Never really used by CRESTA....
   void DefineMatrix(const G4String&, G4int, std::vector<G4double>);

   void DefineVariable(const G4String&, G4double); ///< Set a Variable.
   void SetVariable(const G4String&, G4double); ///< Update a Variable.
   G4bool IsVariable(const G4String&) const; ///< Check if constant or not.

   G4String SolveBrackets(const G4String&); ///< Expand user expression.

   G4double Evaluate(const G4String&); ///< Main evaluation function.

   G4int EvaluateInteger(const G4String&); ///< Assume expression an int.

   /// Grab a string variable. Requires name to end with "_str" or "_string"
   G4String EvaluateString(const G4String& in);

   G4double GetConstant(const G4String&); ///< Get value of constant.
   G4double GetVariable(const G4String&); ///< Get value of variable.

   G4String ConvertToString(G4int ival); ///< Convert an int to string.
   G4String ConvertToString(G4double dval); ///< Convert a double to string.

   void PrintConstants(); ///< Show on screen all variables available to user.
   std::string GetConstantList(); ///< Get string of all variables and values.

   /// Helper function to check string ends with value.
   bool ends_with(std::string const & value, std::string const & ending);


 private:

   G4Evaluator eval; ///< Core Geant4 evaluator class.

   std::vector<G4String> variableList; ///< List of all variables.

   std::vector<G4String> doubleName; ///< List of all double names.
   std::vector<G4String> doubleOriginal; ///< Original expression for doubles.
   std::vector<G4double> doubleValue; ///< Evaluated value for doubles.

   std::vector<G4String> stringName; ///< List of all string names.
   std::map<G4String,G4String> stringValue; ///< List of all string expressions.

};

// ---------------------------------------------------------------------------
} // - namespace DataBase
} // - namespace CRESTA
#endif
