// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with NUISANCE.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "DBEvaluator.hh"
#include "DB.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace DataBase { // CRESTA Database Sub Namespace
// ---------------------------------------------------------------------------

DBEvaluator::DBEvaluator()
{
  eval.clear();
  eval.setStdMath();
  eval.setSystemOfUnits(meter, kilogram, second, ampere, kelvin, mole, candela);

  // Should register variables from inside the database here
  std::vector<DBTable*> dtbls = DB::Get()->GetLinkGroup("VARIABLE");

  for (uint i = 0; i < dtbls.size(); i++) {

    std::vector<std::string> fields = dtbls[i]->GetFields();
    for (uint j = 0; j < fields.size(); j++) {
      if (fields[j].compare("index")==0) continue;
      if (fields[j].compare("name")==0) continue;
      std::string fieldval = fields[j];
      // if (fields[j].compare("type")==0) continue;
      bool stringmode = false;
      if ((ends_with(fieldval, "_str") ||
          ends_with(fieldval, "_string"))) stringmode = true;

      if (stringmode){
        std::string strval = dtbls[i]->GetS(fields[j],true);
        stringName.push_back(fieldval);
        stringValue[fieldval] = strval;

      } else {

        std::string strval = dtbls[i]->GetS(fields[j],true);
        G4double dbval = Evaluate(strval);

        doubleName.push_back(fieldval);
        doubleOriginal.push_back(strval);
        doubleValue.push_back(dbval);

        DefineConstant(fields[j], dbval);
      }
    }
  }
}

DBEvaluator::~DBEvaluator(){
  Clear();
}

void DBEvaluator::Clear()
{
  eval.clear();
  eval.setStdMath();
  eval.setSystemOfUnits(meter, kilogram, second, ampere, kelvin, mole, candela);

  variableList.clear();
}

void DBEvaluator::PrintConstants(){
  for (int i = 0; i < doubleName.size(); i++){
    std::cout << "double : " << doubleName[i] << " -> " << doubleOriginal[i] << " = " << doubleValue[i] << std::endl;
  }
  for (int i = 0; i < stringName.size(); i++){
    std::cout << "string : " << stringName[i] << " = " << stringValue[stringName[i]] << std::endl;
  }
}

std::string DBEvaluator::GetConstantList(){
  std::ostringstream constlist;
  for (int i = 0; i < doubleName.size(); i++){
    constlist <<  doubleName[i] << " " << doubleOriginal[i] << " " << doubleValue[i] << std::endl;
  }
  for (int i = 0; i < stringName.size(); i++){
    constlist <<  stringName[i] << " " << stringValue[stringName[i]] << std::endl;
  }
  return constlist.str();
}

void DBEvaluator::DefineConstant(const G4String& name, G4double value)
{
  // if (eval.findVariable(name))
  // {
    // G4String error_msg = "Redefinition of constant or variable: " + name;
    // G4Exception("DBEvaluator::DefineConstant()", "InvalidExpression",
                // FatalException, error_msg);
  // }
  eval.setVariable(name.c_str(), value);
}

void DBEvaluator::DefineVariable(const G4String& name, G4double value)
{
  // if (eval.findVariable(name))
  // {
    // G4String error_msg = "Redefinition of constant or variable: " + name;
    // G4Exception("DBEvaluator::DefineVariable()", "InvalidExpression",
                // FatalException, error_msg);
  // }
  eval.setVariable(name.c_str(), value);
  variableList.push_back(name);
}

void DBEvaluator::DefineMatrix(const G4String& name,
                               G4int coldim,
                               std::vector<G4double> valueList)
{
  const G4int size = valueList.size();

  if (size == 0)
  {
    G4String error_msg = "Matrix '" + name + "' is empty!";
    G4Exception("DBEvaluator::DefineMatrix()", "InvalidSize",
                FatalException, error_msg);
  }

  if (size % coldim != 0)
  {
    G4String error_msg = "Matrix '" + name + "' is not filled correctly!";
    G4Exception("DBEvaluator::DefineMatrix()", "InvalidSize",
                FatalException, error_msg);
  }

  if ((size == coldim) || (coldim == 1))   // Row- or column matrix
  {
    for (G4int i = 0; i < size; i++)
    {
      std::stringstream MatrixElementNameStream;
      MatrixElementNameStream << name << "_" << i;
      DefineConstant(MatrixElementNameStream.str(), valueList[i]);
    }
  }
  else   // Normal matrix
  {
    const G4int rowdim = size / coldim;

    for (G4int i = 0; i < rowdim; i++)
    {
      for (G4int j = 0; j < coldim; j++)
      {
        std::stringstream MatrixElementNameStream;
        MatrixElementNameStream << name << "_" << i << "_" << j;
        DefineConstant(MatrixElementNameStream.str(), valueList[coldim * i + j]);
      }
    }
  }
}

void DBEvaluator::SetVariable(const G4String& name, G4double value)
{
  if (!IsVariable(name))
  {
    G4String error_msg = "Variable '" + name + "' is not defined!";
    G4Exception("DBEvaluator::SetVariable()", "InvalidSetup",
                FatalException, error_msg);
  }
  eval.setVariable(name.c_str(), value);
}

G4bool DBEvaluator::IsVariable(const G4String& name) const
{
  const size_t variableCount = variableList.size();

  for (size_t i = 0; i < variableCount; i++)
  {
    if (variableList[i] == name)  { return true; }
  }

  return false;
}

G4String DBEvaluator::SolveBrackets(const G4String& in)
{
  std::string::size_type full  = in.size();
  std::string::size_type open  = in.find("[", 0);
  std::string::size_type close = in.find("]", 0);

  if (open == close) { return in; }

  if ((open > close) || (open == std::string::npos) || (close == std::string::npos))
  {
    G4String error_msg = "Bracket mismatch: " + in;
    G4Exception("DBEvaluator::SolveBrackets()", "InvalidExpression",
                FatalException, error_msg);
    return in;
  }

  std::string::size_type begin = open;
  std::string::size_type end = 0;
  std::string::size_type end1 = 0;
  std::string out;
  out.append(in, 0, open);

  do  // Loop for all possible matrix elements in 'in'
  {
    do   // SolveBrackets for one matrix element
    {
      end = in.find(",", begin + 1);
      end1 = in.find("]", begin + 1);
      if (end > end1)                { end = end1; }
      if (end == std::string::npos)  { end = close;}

      std::stringstream indexStream;
      indexStream << "_" << EvaluateInteger(in.substr(begin + 1, end - begin - 1)) - 1;

      out.append(indexStream.str());

      begin = end;

    } while (end < close);

    if (full == close) { return out; }

    open  = in.find("[", begin);
    close = in.find("]", begin + 1);

    if (open == close) {
      out.append(in.substr(end + 1, full - end - 1)); return out;
    }
    out.append(in.substr(end + 1, open - end - 1));

    begin = open;

  } while (close < full);

  return out;
}

G4double DBEvaluator::Evaluate(const G4String& in)
{
  G4String expression = SolveBrackets(in);
  G4double value = 0.0;

  if (!expression.empty())
  {
    value = eval.evaluate(expression.c_str());

    if (eval.status() != G4Evaluator::OK)
    {
      eval.print_error();
      G4String error_msg = "Error in expression: " + expression;
      G4Exception("DBEvaluator::Evaluate()", "InvalidExpression",
                  FatalException, error_msg);
    }
  }
  return value;
}



G4int DBEvaluator::EvaluateInteger(const G4String& expression)
{
  // This function is for evaluating integer expressions,
  // like loop variables and matrix indices.
  // Complains if the evaluated expression has a fractional
  // part different from zero

  G4double value = Evaluate(expression);

  G4int whole = (G4int)value;
  G4double frac = value - (G4double)whole;

  if (frac != 0.0)
  {
    G4String error_msg = "Expression '" + expression
                         + "' is expected to have an integer value!";
    G4Exception("DBEvaluator::EvaluateInteger()", "InvalidExpression",
                FatalException, error_msg);
  }
  return whole;
}

G4double DBEvaluator::GetConstant(const G4String& name)
{
  if (IsVariable(name))
  {
    G4String error_msg = "Constant '" + name
                         + "' is not defined! It is a variable!";
    G4Exception("DBEvaluator::GetConstant()", "InvalidSetup",
                FatalException, error_msg);
  }
  if (!eval.findVariable(name))
  {
    G4String error_msg = "Constant '" + name + "' is not defined!";
    G4Exception("DBEvaluator::GetConstant()", "InvalidSetup",
                FatalException, error_msg);
  }
  return Evaluate(name);
}

G4double DBEvaluator::GetVariable(const G4String& name)
{
  if (!IsVariable(name))
  {
    G4String error_msg = "Variable '" + name + "' is not a defined!";
    G4Exception("DBEvaluator::GetVariable()", "InvalidSetup",
                FatalException, error_msg);
  }
  return Evaluate(name);
}

G4String DBEvaluator::ConvertToString(G4int ival)
{
  std::ostringstream os;
  os << ival;
  G4String vl = os.str();
  return vl;
}

G4String DBEvaluator::ConvertToString(G4double dval)
{
  std::ostringstream os;
  os << dval;
  G4String vl = os.str();
  return vl;
}

G4String DBEvaluator::EvaluateString(const G4String& in){
  if (stringValue.find(in) != stringValue.end()){
    return stringValue[in];
  } else{
    return in;
  }
}

bool DBEvaluator::ends_with(std::string const & value,
                            std::string const & ending)
{
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

// ---------------------------------------------------------------------------
} // - namespace DataBase
} // - namespace CRESTA
