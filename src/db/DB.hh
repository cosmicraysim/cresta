// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with NUISANCE.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef __CRESTA_DB_HH__
#define __CRESTA_DB_HH__
#include "SystemHeaders.hh"

#include "DBTable.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace DataBase { // CRESTA Database Sub Namespace
// ---------------------------------------------------------------------------

/// \class DB
/// \brief Globally Accessible Data Base.
///
/// User accessible database class.
/// Loads multiple config files from predifined locations
/// and makes them accessible throughout the code using
/// a singleton -> DB::Get();
class DB {
public:
  DB(); //< Constructor
  ~DB(); //< Destructor
  DB(std::string filename);

  static DB *Get(); //< Static Access Function
  static DB *fPrimary; //< Static Access Object

  /// Prints CRESTA Logo
  static void PrintSplashScreen();

  /// Returns user ENV variable for main data path folder CRESTA/data/
  static std::string GetDataPath(std::string dbtype="CRESTA");
  /// Returns user ENV variable for main sim path folder CRESTA/simulations/
  static std::string GetSimulationPath(std::string dbtype="CRESTA");

  /// Gets a list of all paths CRESTA should look for dynamic libraries in.
  /// Used for CRESTA extension modules.
  std::vector<std::string> GetDynamicLibraryPath();

  /// Creates a database with a given id.
  void CreateDataBase(std::string dataid);
  /// All DBTables are saved in an selected "database", allowing
  /// data config files to be temporarily loaded and compared.
  void SelectDataBase(std::string dataid);

  /// Check the database has any DBTables of a certain name.
  bool HasTables(std::string name);
  /// Check the database has a specific DBTable with given name and index.
  bool HasTable(std::string name, std::string index);

  /// Load a series of files into current database.
  int Load(std::string filename);
  /// Load all files matching a specific pattern into current database.
  int LoadAll(std::string dirname, std::string pattern = "/*");
  /// Load a single file into current database.
  int LoadFile(std::string filename);

  /// Final cross check of loaded database files, needed when new
  /// files are loaded to check for overlaps, etc.
  void Finalise();

  /// Add a DBTable object to the current database.
  void AddTable(DBTable tbl);
  /// Returns a DBTable given its name and index from current database.
  DBTable GetTable (std::string tablename, std::string index);
  /// Creates a blank DBTable in current database.
  DBTable CreateTable (std::string tablename, std::string index);
  /// Gets all DBTable objects with the given name.
  std::vector<DBTable> GetTableGroup (std::string tablename);

  /// Returns a pointer to a table instead of a copy. Use this when
  /// you want to directly edit the global database. Use with caution.
  DBTable* GetLink  (std::string tablename, std::string index);
  /// Returns pointers to all DBTable objects with the given name.
  /// Use when you want to directly edit the global database. Use with caution.
  std::vector<DBTable*> GetLinkGroup  (std::string tablename);

  /// Get a line seperated list of all currently loaded global variables.
  /// These variables are used in Eval functions, such as in table.GetG4D("value").
  std::string GetConstantList();
  /// Print values of all loaded global variables.
  void PrintConstants();
  /// Add a global parameter to the VARIABLE constants tree.
  void AddGlobalParameter(std::string field, std::string value);

  /// Setup a globally accessible output file name (bit of a hack...)
  inline void SetOutputFile(std::string filename) { fOutputFile = filename; };
  /// Get the globally accessible output file name (bit of a hack...)
  inline std::string GetOutputFile() { return fOutputFile; };

protected:

  /// Map of all DB Tables for a given database id.
  std::map<std::string, std::vector<DBTable> > fAllTables;

  /// Pointer to currently loaded tables.
  std::vector<DBTable>* fCurrentTables;
  /// Pointer to the main default tables, which are the
  /// core CRESTA/data/ folder tables loaded at run time.
  /// If a given table is not found in CurrentTables, the DB
  /// looks here instead.
  std::vector<DBTable>* fDefaultTables;

  DBTable fNullTable; ///< Empty table copy to return.
  int fDataBaseCalls; ///< Counter to track database calls and warn if high.
  std::string fOutputFile; ///< Global Access Filename Hack.

};

// ---------------------------------------------------------------------------
} // - namespace db
} // - namespace CRESTA
#endif
