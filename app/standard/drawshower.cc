#include "TH1D.h"

#include <iostream>
#include <fstream>
#include <cstdlib>

#include <TCanvas.h>
#include <TROOT.h>
#include <TApplication.h>
#include "TFile.h"
#include "TH1D.h"
#include "TBranch.h"
#include "TTree.h"
#include "TH2D.h"
#include "TRandom.h"

int main(int argc, char* argv[]){


  std::string filename = std::string(argv[1]);
  std::cout << "Opening : " << filename << std::endl;
  TFile* infile = new TFile(filename.c_str());


  TApplication app("app", &argc, argv);

  TTree* events = (TTree*) infile->Get("detectorevents");
  std::vector<float>* xvec = 0;
  std::vector<float>* zvec = 0;
  std::vector<float>* evec = 0;

  events->SetBranchAddress("box_x", &xvec);
  events->SetBranchAddress("box_z", &zvec);
  events->SetBranchAddress("box_edep", &evec);

  TH2D* hist = new TH2D("hist","hist",42,-0.075,0.075,400,-0.45,0.45);

  for (int i = 0; i < events->GetEntries(); i++){
    events->GetEntry(i);

    for (int j = 0; j < xvec->size(); j++){
      if (j % 10000 == 0) std::cout << j << std::endl;
      float x = xvec->at(j)/1000.0;
      float z = zvec->at(j)/1000.0;
      float e = evec->at(j);      
      //      std::cout << x << " " << z << " " << e << std::endl;
      hist->Fill(x,z,100.0);

      continue;

      for (int k = 0; k < 100; k++){
	hist->Fill(x + gRandom->Gaus(0.0,0.0001), z + gRandom->Gaus(0.0,0.0001), e);
      }
      
      float offx = gRandom->Gaus(0.0,0.0003);
      float offz = gRandom->Gaus(0.0,0.0003);
      float width = e/10000.0;
	 
      for (int k = 0; k< 100; k++){
	hist->Fill(x + offx + gRandom->Gaus(0.0,width), z + offz + gRandom->Gaus(0.0,width), e*0.2);
	offx +=  gRandom->Gaus(0.0,width*0.1);
	offz +=  gRandom->Gaus(0.0,width*0.1);
      }
    }
  }

  TCanvas* c1 = new TCanvas("c1","c1",800,600);
  c1->cd();
  hist->Draw("COLZ");
  c1->Update();

  app.Run(kTRUE);
}
