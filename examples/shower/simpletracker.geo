// ---------------------------------------------------
// Global Config
{
  name: "GLOBAL",
  index: "config",
  world: "cresta",
  flux: "gaussianbeamsource",
  physics: "Shielding",
  batchcommands: "",
  action: "cresta"
}

{
  name: "VARIABLE"
  index: "config"
  IONZ: "6"
  IONA: "12"
}

{
  name: "GaussianBeamSource",
  index: "config",
  direction: ["0.0","0.0","-1.0"]
  position: ["0*m","0.0","+20.0*m"]
  fwhmposition: ["0.5*cm","0.5*cm","0.0"]
  particle_energy_pdf: "1"
  particle_energy_min: "100*GeV"
  particle_energy_max: "101*GeV"
  particle_ids: ["generic_ion_0"]
  generic_ion_0_Z: "IONZ"
  generic_ion_0_A: "IONA"
  generic_ion_0_E: "0*MeV"
  particle_probs: ["1.0"]
}

// ---------------------------------------------------
// World Geometry : 20 x 20 x 30 AIR
// Then air and carbon base
{
  name: "GEO",
  index: "world",
  material: "G4_AIR",
  size: ["3.01*m", "30.01*m", "40.*m"],
  type: "box",
  color: [1.0,1.0,1.0],
}

{
  name: "DETECTOR"
  index: "trueparticlevector"
  type: "trueparticlevector"
}

{
  name: "GEO",
  index: "box",
  mother: "world"
  material: "G4_POLYSTYRENE",
  size: ["3.*m", "30.0*m", "39.5*m"],
  type: "box",
  color: [1.0,1.0,1.0],
  sensitive: "trueparticlevector"
}