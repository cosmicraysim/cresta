// ---------------------------------------------------
// Global Config
// ---------------------------
// Need to tell CRESTA you are using, world: optical, physics: wlsoptical, action: optical
{
  name: "GLOBAL",
  index: "config",
  world: "optical",
  flux: "parrallel",
  physics: "wlsoptical",
  batchcommands: "",
  action: "optical"
}

// ---------------------------
// Beam definition
// ---------------------------
// Lots of electrons
{
  name: "ParrallelBeam",
  index: "config",
  energy_min: "1*MeV"
  energy_max: "10000*MeV"
  direction: ["0.0","1.0","0.0"]
  particle_id: "e-"
}

{  name: "FLUX",  
   index: "source_box",  
   size: ["10*mm", "100*mm", "100*mm"],  
   position: ["0.0", "-250*mm","0.0"] 
}

// ---------------------------------------------------
// World Geometry : 3 x 3 x 3 G4_AIR
// ---------------------------
// Then air and carbon base
{
  name: "GEO",
  index: "world",
  material: "G4_AIR",
  size: ["3.*m", "3.0*m", "3.*m"],
  type: "box",
  color: [1.0,1.0,1.0],
}

// Need to make an outer tube that has optical settings so we can consider
// light loss from scintillator to air.
{
  name: "GEO",
  index: "outertube",
  type: "tubs",
  mother: "world",
  material: "OPTICAL_AIR"
  size_z: "100*cm"
  r_max: "100*cm"
  color: [1.0,0.5,1.0,0.0001]
}


// ---------------------------
// Scintillator Placement.
// ---------------------------
// Emission properties in EJ200 definition. See data/materials/
{
  name: "GEO",
  index: "scint",
  type: "tubs",
  mother: "outertube",
  material: "EJ200"
  size_z: "30*cm"
  r_max: "7*cm"
  position: ["0.0","0.0","0.0"],
  color: [0.0,0.0,1.0,0.3]
  drawstyle: "solid"
}


// ---------------------------
// PMT PLACEMENT
// ---------------------------
// Make a photonsd which saves a vector of photon arrival times.
{
  name: "DETECTOR"
  index: "photon"
  type: "photonsd"
}

// Place a PMT at bottom
{
  name: "GEO"
  index: "pmt1"
  type: "tubs"
  size_z: "2*cm"
  r_max: "7*cm"
  mother: "scint"
  position: ["0.0","0.0","-15*cm-1*cm"]
  material: "OPTICAL_AIR"
  color: [1.0,0.0,0.0,0.8]
  sensitive: "photon"
  enable : 1
}

// Place a PMT at top
{
  name: "GEO"
  index: "pmt2"
  type: "tubs"
  size_z: "2*cm"
  r_max: "7*cm"
  mother: "scint"
  position: ["0.0","0.0","+15*cm+1*cm"]
  material: "OPTICAL_AIR"
  color: [1.0,0.0,0.0,0.8]
  sensitive: "photon"
  enable : 1
}


// ---------------------------
// OPTICAL SURFACES 
// ---------------------------
// Main Template to just force efficiency reflectivity to 1 for all energies.
// Make seperate tables if specific properties needed.
{
  enable: "0"
  name: "OPTICAL"
  index: "optical1"
  type: "opticalsurface"
  surface: "dielectric_dielectric"
  finish: "polished"
  model: "glisur"
  PROPERTIES_X: ["0.1*eV","1000*eV"]
  REFLECTIVITY_Y: ["1.0","1.0"]
  EFFICIENCY_Y: ["1.0","1.0"]
}

// Now clone templates for both in and out, think both are needed...
{ name: "OPTICAL", index: "scint_air", clone: "optical1", inner: "scint", outer: "outertube" enable: "1" }
{ name: "OPTICAL", index: "air_scint", clone: "optical1", outer: "scint", inner: "outertube" enable: "1" }

{ name: "OPTICAL", index: "pmt1_air", clone: "optical1", inner: "pmt1", outer: "outertube" enable: "1" }
{ name: "OPTICAL", index: "air_pmt1", clone: "optical1", outer: "pmt1", inner: "outertube" enable: "1" }

{ name: "OPTICAL", index: "pmt2_air", clone: "optical1", inner: "pmt2", outer: "outertube" enable: "1" }
{ name: "OPTICAL", index: "air_pmt2", clone: "optical1", outer: "pmt2", inner: "outertube" enable: "1" }