from ROOT import *

infile = TFile("detector/detector.0.0.root","READ")
events = infile.Get("detectorevents")

# Draw Edep in MeV
events.Draw("tracker_E","tracker_E > 0.01","HIST")
gPad.Update()
raw_input("Edep")

# Draw energy dep position
events.Draw("tracker_x","tracker_E > 0.01","HIST")
gPad.Update()
raw_input("X pos")
