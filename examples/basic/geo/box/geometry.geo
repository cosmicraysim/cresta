// ---------------------------------------------------
// Global Config
{
  name: "GLOBAL",
  index: "config",
  flux: "cry",
  world: "cresta",
  action: "cresta",
  physics: "QGSP_BERT"
}

// ---------------------------------------------------
// World Geometry : 3 x 3 x 3 AIR
// Then air and carbon base
{
  name: "GEO",
  index: "world",
  material: "G4_AIR",
  size: ["3.*m", "3.0*m", "2.*m"],
  type: "box",
  color: [1.0,1.0,1.0],
}

// ---------------------------------------------------
// Flux Generator Source Location : Default is Shukla
{
  name: "FLUX",
  index: "source_box",
  size: ["3.0*m", "3.0*m", "0.05*m"],
  position: ["0.0","0.0", "0.975*m"],
}

//--------------------------------------------
// Main Box Geometry, placed inside mother WORLD
{
  name: "GEO",
  index: "tracker",
  type: "box",

  mother: "world",

  material: "G4_CONCRETE"

  size: ["40*cm","80*cm","40*cm"],

  position: ["0.0","0.0","0.0"],
}

