// ---------------------------------------------------
// Global Config
{
  name: "GLOBAL",
  index: "config",
  flux: "parrallel",
  world: "cresta",
  action: "cresta",
  physics: "QGSP_BERT"
}

// ---------------------------------------------------
// World Geometry : 3 x 3 x 3 AIR
// Then air and carbon base
{
  name: "GEO",
  index: "world",
  material: "G4_AIR",
  size: ["30.*cm", "30.0*cm", "30.*cm"],
  type: "box",
  color: [1.0,1.0,1.0,0.0],
}

// ---------------------------------------------------
// Flux Generator Source Location : Default is Shukla

{
  name: "ParrallelBeam",
  index: "config",
  energy_min: "100*eV"
  energy_max: "1000*eV"
  direction: ["1.0","-1.0","-1.0"]
  particle_id: "neutron"
}

{  name: "FLUX",
   index: "source_box",
   size: ["20*mm", "20*mm", "1*mm"],
   position: ["-15.0*cm", "+15.0*cm","15.0*cm+2.0*cm"]
}

//--------------------------------------------
// Main Box Geometry, placed inside mother WORLD
{
  name: "GEO",
  index: "block111",
  type: "foilwrappedblock",
  mother: "world",
  position: ["0.0","0.0","0.0"],
}

{ name: "GEO", index: "block112" clone: "block111", position: ["0.0","0.0","1.5*cm"] }
{ name: "GEO", index: "block113" clone: "block111", position: ["0.0","0.0","3.0*cm"] }
{ name: "GEO", index: "block114" clone: "block111", position: ["0.0","0.0","4.5*cm"] }

{ name: "GEO", index: "block121" clone: "block111", position: ["0.0","5.0*cm","0.0*cm"] }
{ name: "GEO", index: "block122" clone: "block111", position: ["0.0","5.0*cm","1.5*cm"] }
{ name: "GEO", index: "block123" clone: "block111", position: ["0.0","5.0*cm","3.0*cm"] }
{ name: "GEO", index: "block124" clone: "block111", position: ["0.0","5.0*cm","4.5*cm"] }

{ name: "GEO", index: "block211" clone: "block111", position: ["5.0*cm","0.0","0.0*cm"] }
{ name: "GEO", index: "block212" clone: "block111", position: ["5.0*cm","0.0","1.5*cm"] }
{ name: "GEO", index: "block213" clone: "block111", position: ["5.0*cm","0.0","3.0*cm"] }
{ name: "GEO", index: "block214" clone: "block111", position: ["5.0*cm","0.0","4.5*cm"] }

{ name: "GEO", index: "block221" clone: "block111", position: ["5.0*cm","5.0*cm","0.0*cm"] }
{ name: "GEO", index: "block222" clone: "block111", position: ["5.0*cm","5.0*cm","1.5*cm"] }
{ name: "GEO", index: "block223" clone: "block111", position: ["5.0*cm","5.0*cm","3.0*cm"] }
{ name: "GEO", index: "block224" clone: "block111", position: ["5.0*cm","5.0*cm","4.5*cm"] }