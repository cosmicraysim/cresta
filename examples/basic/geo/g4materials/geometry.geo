// ---------------------------------------------------
// Global Config
{
  name: "GLOBAL",
  index: "config",
  flux: "cry",
  world: "cresta",
  action: "cresta",
  physics: "QGSP_BERT"
}

// ---------------------------------------------------
// World Geometry : 3 x 3 x 3 AIR
// Then air and carbon base
{
  name: "GEO",
  index: "world",
  material: "G4_AIR",
  size: ["3.*m", "3.0*m", "2.*m"],
  type: "box",
  color: [1.0,1.0,1.0],
}

// ---------------------------------------------------
// Flux Generator Source Location : Default is Shukla
{
  name: "FLUX",
  index: "source_box",
  size: ["3.0*m", "3.0*m", "0.05*m"],
  position: ["0.0","0.0", "0.975*m"],
}

//--------------------------------------------
// 4 different boxes, of different materials.
{
  name: "GEO",
  index: "box_concrete",
  type: "box",

  mother: "world",

  material: "G4_CONCRETE"

  size: ["30*cm","30*cm","30*cm"],

  position: ["+30.0*cm","0.0","0.0"],
}

{
  name: "GEO",
  index: "box_lead",
  type: "box",

  mother: "world",

  material: "G4_Pb"

  size: ["30*cm","30*cm","30*cm"],

  position: ["-30.0*cm","0.0","0.0"],
  
  color: [0.0,0.0,1.0]
  drawstyle: "solid"
}

{
  name: "GEO",
  index: "box_iron",
  type: "box",

  mother: "world",

  material: "G4_Fe"

  size: ["30*cm","30*cm","30*cm"],

  position: ["0.0*cm","-30.0*cm","0.0"],
  
  color: [1.0,0.0,0.0]
  drawstyle: "solid"
}

{
  name: "GEO",
  index: "box_poly",
  type: "box",

  mother: "world",

  material: "G4_POLYSTYRENE"

  size: ["30*cm","30*cm","30*cm"],

  position: ["0.0*cm","+30.0*cm","0.0"],

  color: [0.0,1.0,0.0]
  drawstyle: "solid"
}