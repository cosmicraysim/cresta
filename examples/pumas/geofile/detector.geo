// ---------------------------------------------------
// Global Config
{
  name: "GLOBAL",
  index: "config",
  flux: "pumasbackwards",
  world: "cresta",
  action: "cresta",
  physics: "QGSP_BERT"
}

{
  name: "PUMAS",
  index: "config",
  position: ["0.0","0.0","-50*m+5*m"]
}

// ---------------------------------------------------
// World Geometry : 3 x 3 x 3 AIR
// Then air and carbon base
{
  name: "GEO",
  index: "world",
  material: "G4_AIR",
  size: ["300.*m", "300.0*m", "100.*m"],
  type: "box",
  color: [1.0,1.0,1.0],
}

// ---------------------------------------------------
// Flux Generator Source Location : Default is Parrallel
{
  name: "ParrallelBeam",
  index: "config",
  energy_min: "100000*eV"
  energy_max: "1000000*eV"
  direction: ["0.0","0.0","-1.0"]
  particle_id: "gamma"
}

{
  name: "FLUX",
  index: "source_box",
  size: ["0.1*m", "0.1*m", "0.05*m"],
  position: ["0.0","0.0", "+40.0*cm"],
}

//--------------------------------------------
// Main Box Geometry, placed inside mother WORLD

{
  name: "DETECTOR"
  index: "scintillator"
  type: "scintillator"
  efficiency: 0.85
}

{
  name: "GEO",
  index: "rock",
  type: "box",

  mother: "world",

  material: "G4_CONCRETE"

  size: ["300*m","300*m","80*m"],

  position: ["0.0","0.0*m","-50.0*m + 50*m"],

  color: [0.0,0.0,1.0]
}

{
  name: "GEO",
  index: "target",
  type: "box",

  mother: "rock",

  material: "G4_AIR"

  size: ["10*m","10*m","10*m"],

  position: ["0.0","+15.0*m","0.0"],

  color: [0.0,0.0,1.0]
}


{
  name: "GEO",
  index: "target2",
  type: "box",

  mother: "rock",

  material: "G4_Pb"

  size: ["10*m","10*m","10*m"],

  position: ["0.0","-15.0*m","0.0"],

  color: [0.0,0.0,1.0]
}

{
  name: "GEO",
  index: "tracker",
  type: "box",

  mother: "world",

  material: "G4_POLYSTYRENE"

  size: ["200*cm","200*cm","200*cm"],

  position: ["0.0","0.0","-50.0*m + 20*cm"],
  
  color: [0.0,0.0,1.0]

  sensitive: "scintillator"
}

